# Synopsis

Gilmi est développeur chez Tunnel Corp., il développe une application en PHP qui doit stocker des données de type clé/valeur dans une base de données. Cependant Gilmi qui aime beaucoup la bière mais pas trop le sql ne voudrait pas avoir à créer une base de données et à entendu parler de Redis.

Cependant Ganfald le grand administrateur blanc, n'est pas trop dispo en ce moment pour lui expliquer comment installer Redis. 

Cependant Saroum le grand patron aimerait bien que l'application soit rapidement finalisée pour pouvoir remplir d'or les coffres de la société.

Gilmi à entendu parler de Docker et il se dit qu'il pourrait sûrement s'installer un conteneur Redis pour finir son dev et donner le tout à Ganfald pour qu'il mette en prod par la suite.


# Pour commencer Gilmi lit quelques grimoires sur Docker

Deux présentations dans le dossier presentation :
* Docker Bday #4 - Intro deck.odp : présentation officielle lors du birthday Docker 2017
* workshop_decouverte.odp : la présentation faite par K' Rément Libre, librement téléchargeable et réutilisable

# La quête commence

Suivant le système sur lequel Gilmi va dévelpper il existe différentes façon d'installer Docker :

**https://store.docker.com/search?offering=community&type=edition**

La meilleure façon de tester si Docker est fonctionnel est de faire un *Hello World*

    # docker container run hello-world

L'image hello-world à été récupérée du registre, puis un contener à été créé, exécuté et arrêté.

Remarquez la différence entre :
   
    # docker container list 
    # docker container list -a

Idem avec 
   
    # docker container ps
    # docker container ps -a

# Le grimoire aux images

Les images de conteneurs peuvent se retrouver dans plusieurs *grimoires*, dans la nomenclature docker, on parle de registre. Celui utilisé par défaut est le docker hub : http://hub.docker.com , il est possible d'y faire des recherches directement pour trouver des images. 
Il est aussi possible d'y faire des recherches via la ligne de commande :

    # docker search nginx

Un nouveau *grimoire* est en cours d'édition : http://store.docker.com


 
# Snaug le gardien du html

*"Le code HTML tu dois afficher si ta quête tu veux continuer"*

Une des premières étapes est d'afficher du code html puis PHP. Il va donc faloir créer un conteneur pour gérer le service web et un autre pour le processus PHP.

Par facilité on va prendre du nginx.

Il faut donc ocmmencer par déployer un conteneur nginx, ce conteneur doit pouvoir lire le code en local sur le serveur.

Première chose on récupère l'image nginx :

    # docker image pull nginx

    # docker image list 
    REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
    nginx               latest              5766334bdaa0        8 days ago          183MB

Récupérer sur le git le code de tout le projet :
    # git clone ....

On a le code html de notre page dans **code/index.html**


La configuration basique de nginx dans **conf/nginx/site-html.conf**

	

Démarrer le conteneur :

    # docker container run --name web01 --hostname web01 -p 80:80 -v ${PWD}/code:/var/www/html -v ${PWD}/conf/nginx/site-html.conf:/etc/nginx/conf.d/default.conf -d nginx

Allez visiter l'ip de votre serveur pour vérifier que vous avez vaincu Snaug

Si vous voulez aller visiter le contenu du contener :

    # docker exec -it web01 /bin/bash

Pour en sortir : exit


# Galum et le PHP


*"Il me faut le précieux PHP "*


On va démarrer un conteneur spécifique paramétré en php 5 avec le support redis :

L'image PHP supportant Redis se trouve sur un registre spécifique, pour la récupérer :
    
    # docker pull registry.containers.ovh.net/hisyl/php5.6-fpm-redis

Pour démarrer le conteneur 
    
    # docker container run --name php -d -v ${PWD}/code:/var/www/html registry.containers.ovh.net/hisyl/php5.6-fpm-redis

On supprimer le conteneur web actuel pour le remplacer 

    # docker container rm -f web01

On démarre le contener nginx linké au conteneur php5 :
    
    # docker container run --name web01 --hostname web01 -p 80:80 -v ${PWD}/code:/var/www/html -v ${PWD}/conf/nginx/site-php.conf:/etc/nginx/conf.d/default.conf --link php:php -d nginx

Afficher la page dans la navigateur, la mission est réussie ?


## Est-ce que mon code fonctionne en php7 ?

On récupère l'image php7

    # docker pull registry.containers.ovh.net/hisyl/php7.0-fpm-redis

On supprime le conteneur PHP 5 :
    # docker container rm -f php

On démarre le conteneur PHP 7 :

    # docker container run --name php -d -v ${PWD}/code:/var/www/html registry.containers.ovh.net/hisyl/php7.0-fpm-redis


Vérifier votre page php via le navigateur et on part en courant sans donner le précieux à Galum




# La fin cette première quête 

Reste à Gilmi à mettre en place la pière angulère *redis* :

    # docker run --name redis -d redis

Il faut recréer le conteneur PHP avec le liens vers Redis

    # docker container rm -f php
    # docker container run --name php -d -v ${PWD}/code:/var/www/html --link redis:redis registry.containers.ovh.net/hisyl/php7.0-fpm-redis

Le code à afficher : http://IP_machine/fin.php

