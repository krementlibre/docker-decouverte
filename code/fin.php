<?php

   $time=microtime(true);
   $redis=new Redis();
   $redis->connect('redis',6379);

   // Store informations in Redis
   $redis->set('Developpeur','Gilmi');
   $redis->set('Administrateur systeme','Ganfald');
   $redis->set('Patron','Saroum');
   $redis->set('Dragon','Snaug');
   $redis->set('Pas beau','Galum');

   $dev=$redis->get('Developpeur');
   $admin=$redis->get('Administrateur systeme');
   $patron=$redis->get('Patron');
   $dragon=$redis->get('Dragon');
   $beurk=$redis->get('Pas beau');

   echo "<h3>Merci aux acteurs ... </h3>";
   echo "<p>Notre valeureux développeur :  <strong>$dev</strong></p>";
   echo "<p>Le merveilleux admin système : <strong>$admin</strong> ... qui n'a pas fait grand chose ... </p>";
   echo "<p>Aucun Dragon n'a été brutalisé pendant cette quête, merci à <em>$dragon</em></p>";

   echo "<h3>Gilmi à bien mérité une petite bière ... </h3>";
   echo "<img width='50%' height='50%' src='img/gimli.jpg' />" ;
?>
